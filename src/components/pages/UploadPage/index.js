import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Upload from '../../templates/Upload';
import { uploadImage } from '../../../actions';

const UploadPage = ({uploadImageProp, error, uploadStage, history, loading
    }) => {
        const actionOnComplete = () => history.push('/')

        return (<Upload 
            uploadImage={uploadImageProp} 
            error={error}
            actionOnComplete={actionOnComplete}
            stage={uploadStage}
            loading={loading}
        />)
    }

const mapStateToProps = state => ({
    error: state.appState.error,
    uploadStage: state.appState.uploadStage,
    loading: state.appState.loading
});

const mapDispatchToProps = dispatch => ({
    uploadImageProp: payload => dispatch(uploadImage(payload))
});
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UploadPage));