import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Home from '../../templates/Home';
import { 
 getCatList,
 setfavourites,
 unFavourites,
 getFavourites,
 addVote,
 deleteVote,
 getVotes
} from '../../../actions';

export const HomePage = ({ 
    getCatListProp, 
    catList, 
    stage, 
    setFavourites, 
    setUnFavourites, 
    favourites, 
    getFavouritesProp,
    addVoteProp,
    deleteVoteProp,
    getVotesProp,
    votes,
    loading

}) => {
    const props = {
        getCatListProp,
        catList,
        stage,
        setFavourites,
        setUnFavourites,
        favourites,
        getFavouritesProp,
        addVoteProp,
        deleteVoteProp,
        getVotesProp,
        votes,
        loading
    }
    return <Home {...props}   />
}

const mapStateToProps = state => ({
    catList: state.appState.catList,
    stage: state.appState.stage,
    favourites: state.appState.favourites,
    votes: state.appState.votes,
    loading: state.appState.loading
});

const mapDispatchToProps = dispatch => ({
    getCatListProp: () => dispatch(getCatList()),
    setFavourites: payload => dispatch(setfavourites(payload)),
    setUnFavourites: payload => dispatch(unFavourites(payload)),
    getFavouritesProp: () => dispatch(getFavourites()),
    addVoteProp: payload => dispatch(addVote(payload)),
    deleteVoteProp: payload => dispatch(deleteVote(payload)),
    getVotesProp: () => dispatch(getVotes())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HomePage));