import React from 'react';
import MuiAppBar from '@material-ui/core/AppBar';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '../../atoms/Typography';
import LinkIconButton from '../../atoms/LinkIconButton';

const useStyles = makeStyles(theme => ({
    title: {
        textAlign: "center"
    }
}))

const AppBar = ({ children, link}) => {
    const classes = useStyles();
    return (<MuiAppBar position="fixed"> 
        <Typography 
            weight={500} 
            size={20}
            className={classes.title}
        >
            Waracle React Test
        </Typography>

        <LinkIconButton
            to={link}
            color="inherit"
            variant="contained"
        >
            {children}
        </LinkIconButton>

    </MuiAppBar>)
    };

export default AppBar;


