import React from 'react';
import Grid from '@material-ui/core/Grid';
import shortid from 'shortid';
import Cat from './../../molecules/Cat';

const CatList = props => (
    <Grid container>
        {props.catList && props.catList.length > 0 && props.catList.map(cat => <Grid
            key={shortid.generate()} 
            container
            item
            xs={12}
            md={4}
            >
                <Cat cat={cat} {...props}/>
            </Grid>) }
    </Grid>
)

export default CatList;