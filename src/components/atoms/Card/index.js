import React from 'react';
import { string } from 'prop-types';
import MuiCard from '@material-ui/core/Card';

const Card = ({ children, className }) => (
    <MuiCard className={className}>
        {children}
    </MuiCard>
)

Card.propTypes = {
    className: string,
}

Card.defaultProps = {
    className: '',
}

export default Card