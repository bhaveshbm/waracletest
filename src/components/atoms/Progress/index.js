import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles({
    layOut: {
      padding: 25,
      textAlign: 'center'
    },
  });
const Progress = () => {
    const classes = useStyles();
return (<div className={classes.layOut}>
    <CircularProgress />
    </div>)
}

export default Progress