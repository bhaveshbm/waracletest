import React from 'react';
import MuiIconButton from '@material-ui/core/IconButton';
import RouterLink from '../RouterLink';

const LinkIconButton = props => (
  <MuiIconButton {...props} component={RouterLink} />
);

export default LinkIconButton;