import React from 'react';
import { oneOf, number } from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';

const useStyles = makeStyles({
    root: props => ({
        fontSize: props && props.size,
        fontWeight: props && props.weight,
        margin: 10
    })
})

const Typography = ({ size, weight, className, ...otherProps }) => {
    const classes = useStyles({ size, weight });

    return (
        <MuiTypography {...otherProps} className={`${classes.root} ${className}`} />
    )
}

Typography.propTypes = {
    size: number,
    weight: oneOf([300, 400, 500, 700]),
};

Typography.defaultProps = {};

export default Typography;