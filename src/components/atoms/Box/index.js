import React from 'react';
import { string } from 'prop-types';
import MuiBox from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    boxLayout: {
        margin: 10
    }
  });

const Box = ({ children, className }) => {
    const classes = useStyles();
    return <MuiBox className={`${classes.boxLayout} ${className}`}>
        {children}
    </MuiBox>
}

Box.propTypes = {
    className: string,
}

Box.defaultProps = {
    className: '',
}

export default Box