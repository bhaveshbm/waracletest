import React from 'react';
import MuiButton from '@material-ui/core/Button';

const Button = ({ children, className, onClick}) => <MuiButton 
    className={className} 
    onClick={onClick} 
    variant="contained" 
    color="secondary">
        {children}
</MuiButton>

export default Button;
                


