import React from 'react';
import { string, number } from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: props => ({
    width: props.width ? `${props.width}px` : `${100 * props.scale}%`,
  }),
}));

const Image = ({ src, scale, alt, width, className }) => {
  const classes = useStyles({ scale, width });
  return <img className={`${classes.root} ${className}`} src={src} alt={alt} />;
};

Image.propTypes = {
  alt: string,
  scale: number,
  width: number,
  className: string,
  src: string.isRequired,
};

Image.defaultProps = {
  scale: 1,
  width: 0,
  alt: '',
  className: '',
};

export default Image;