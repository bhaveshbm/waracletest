import React from 'react';
import { string } from 'prop-types';
import MuiContainer from '@material-ui/core/Container';

const Container = ({ children, className }) => (
    <MuiContainer maxWidth="md" className={className}>
        {children}
    </MuiContainer>
)

Container.propTypes = {
    className: string,
}

Container.defaultProps = {
    className: '',
}

export default Container