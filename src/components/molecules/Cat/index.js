import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import Box from '../../atoms/Box';
import Image from '../../atoms/Image';

const useStyles = makeStyles(theme => ({
    root: {
        margin: 20,
        width: "inherit"
    },
    header: {
        textAlign: "center",
        backgroundColor: "rgb(11, 35, 81)",
        color: "#FFFFFF"
    },
    content: {
        textAlign: "center"
    },
    gridLayout: {
        display: 'flex',
        alignItems: 'baseline'
    }
}))

const Cat = props => {
    const { cat,
        setFavourites, 
        setUnFavourites, 
        favourites, 
        votes,
        addVote,
        deleteVote
     } = props;
    const catFavourite = favourites.find(item => item.image_id && item.image_id === cat.id);
    const catVotes = votes.filter(item => item.image_id && item.image_id === cat.id);
    const numberOfVotes = catVotes && catVotes.length > 0 ? catVotes.length : 0
    const classes = useStyles();
    const onClickActionHandler = () => {
        if(catFavourite){
            return setUnFavourites(catFavourite.id);
        } else {
            return setFavourites(cat.id)
        }
    }
    const onClickDownHandler = () => deleteVote(catVotes[0].id);
    const onClickUpHandler = () => addVote(cat.id);

     return (
        <Card className={classes.root}>
            <CardHeader title={cat.original_filename} className={classes.header} />
            <CardContent className={classes.content}>
                <Image src={cat.url} title={cat.original_filename} scale={1} alt={cat.original_filename} />
                <Box>
                    <IconButton onClick={onClickActionHandler}>
                    {
                        catFavourite
                        ? <FavoriteIcon />
                        : <FavoriteBorderIcon />
                    }
                    </IconButton>
                </Box>
                <Box>
                    <Grid container spacing={1} className={classes.gridLayout}>
                        <Grid item xs={3}>
                            {numberOfVotes > 0 && (<IconButton onClick={onClickDownHandler}>
                                <ThumbDownIcon/>
                            </IconButton>)}
                        </Grid>
                        <Grid item xs={6}>
                            Total Votes: {numberOfVotes}
                        </Grid>
                        <Grid item xs={3}>
                            <IconButton onClick={onClickUpHandler}>
                                <ThumbUpIcon />
                            </IconButton>
                        </Grid>
                    </Grid>
                </Box>
            </CardContent>
        </Card> 
    )
}

export default Cat;