import React, {useState, useEffect} from 'react';
import Container from '../../atoms/Container';
import Card from '../../atoms/Card';
import Box from '../../atoms/Box';
import HomeIcon from '@material-ui/icons/Home';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '../../atoms/Typography';
import AppBar from '../../organisms/AppBar';
import Button from '../../atoms/Button';
import { STAGE } from '../../../constants';
import Progress from '../../atoms/Progress';

const useStyles = makeStyles({
    marginFromTop: {
      marginTop: 100
    },
    cardLayout: {
        border: '1px solid black',
        padding: 10
    },
    error: {
        color: 'red'
    },
    boxLayout: {
        margin: 5
    }
  });
  
const Upload = ({uploadImage, error, actionOnComplete, stage, loading}) => {
    const classes = useStyles();
    const [file, setFile] = useState(null);
    useEffect(() => {
        let unmounted = false;
        const callBack = () => {
            if(stage === STAGE.completed) {
                actionOnComplete()
            }
        }
        !unmounted && callBack();
        return () => { unmounted = true};
    }, [stage, actionOnComplete])
    const fileHandler = event => {
        setFile(event.target.files[0]);
    }
    const submit = () => {
        if(file) {
            const data = new FormData() 
            data.append('file', file, file.name)
            uploadImage(data)
        }
    }
    const uploadComponent = (<>
        <Box>
            <input type='file' name='file' onChange={fileHandler} />
        </Box>
        <Box>
            <Button onClick={submit}>Upload</Button>
        </Box>
        <Box>
            <Typography className={classes.error}>{error}</Typography>
        </Box>
    </>)
    return (
        <Container>
            <AppBar link="/">
                <HomeIcon />
            </AppBar>
            <div className={classes.marginFromTop}>
                <Typography variant="h4">Upload Cat Image</Typography>
                <Card className={classes.cardLayout}>
                    {
                        loading 
                        ? <Progress />
                        : uploadComponent
                    }
                </Card>
            </div>       
        </Container>
    )
}

export default Upload;