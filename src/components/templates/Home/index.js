import React, { useEffect } from 'react';
import Container from '../../atoms/Container';
import CatList from '../../organisms/CatList';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '../../organisms/AppBar';
import Progress from '../../atoms/Progress';

const useStyles = makeStyles({
    marginFromTop: {
      marginTop: 100
    },
  });

const Home = ({
    getCatListProp,
    catList,
    stage,
    setFavourites,
    setUnFavourites,
    favourites,
    getFavouritesProp,
    addVoteProp,
    deleteVoteProp,
    getVotesProp,
    votes,
    loading
}) => {
    const classes = useStyles();
    useEffect(() => {
        let unmounted = false;
        const getCatListFn = async () => {
            await getCatListProp();
            await getFavouritesProp();
            await getVotesProp();
        }
        !unmounted && !stage && getCatListFn();
        return () => { unmounted = true};
    },[stage,getCatListProp, getFavouritesProp, getVotesProp])
    return (
        <Container>
            <AppBar link="/upload">
                <CloudUploadIcon />
            </AppBar>
            <div className={classes.marginFromTop}>
                {
                    loading 
                    ? <Progress />
                    : <CatList 
                        catList={catList}
                        setFavourites={setFavourites}
                        setUnFavourites={setUnFavourites}
                        favourites={favourites}
                        addVote={addVoteProp}
                        deleteVote={deleteVoteProp}
                        votes={votes}
                    />
                }
            </div>
        </Container>
    )
}

export default Home;