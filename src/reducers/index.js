import { combineReducers } from 'redux';
import * as constant from '../constants';

const initialState = {
   stage: constant.STAGE.init,
   uploadStage: constant.STAGE.init,
   error: null,
   favourites: [],
   votes: [],
   loading: false
};

export const appState = (state = initialState, action) => {
    const { 
        type,
        payload
    } = action;
    const {
        favourites,
        votes
    } = state;
    switch (type) {
        case constant.UPLOAD_INIT:
        case constant.CAT_LIST_INIT:
            return {
                ...state,
                stage: constant.STAGE.init,
                uploadStage: constant.STAGE.init,
                error: null,
                loading: true
            }
        case constant.CAT_LIST:
            return {
                ...state,
                catList: payload,
                stage: constant.STAGE.completed,
                loading: false
            }
        case constant.UPLOAD_FAIL:
            return {
                ...state,
                error: payload,
                uploadStage: constant.STAGE.fail,
                loading: false
            }
        case constant.UPLOAD_COMPLETE:
            return {
                ...state,
                uploadStage: constant.STAGE.completed,
                loading: false
            }
        case constant.FAVOURITES: 
            return {
                ...state,
                favourites: payload
            }
        case constant.UNFAVOURITES:
            const filteredVotes = favourites.filter(listItem => listItem.id !== payload)
            return {
                ...state,
                favourites: filteredVotes
            }
        case constant.SETFAVOURITES:
            return {
                ...state,
                favourites: [
                    ...favourites,
                    payload
                ]
            }
        case constant.VOTES: 
            return {
                ...state,
                votes: payload
            }
        case constant.DELETE_VOTE:
            const filteredList = votes.filter(listItem => listItem.id !== payload)
            return {
                ...state,
                votes: filteredList
            }
        case constant.ADD_VOTE:
            return {
                ...state,
                votes: [
                    ...votes,
                    payload
                ]
            }
        
        default:
            return state;
    }
};

const rootReducer = combineReducers({
    appState,
})

export default rootReducer;