import * as constants from  '../constants';
import { getRequest, postRequest, deleteRequest, uploadRequest } from '../services/httpService';

const getCatList = () => {
    return dispatch => {
        dispatch({
            type: constants.CAT_LIST_INIT
        })
        getRequest('images')
            .then(response => {
                dispatch({
                    type: constants.CAT_LIST,
                    payload: response
                })
            })
    }
}

const uploadImage = payload => {
    return dispatch => {
        dispatch({
            type: constants.UPLOAD_INIT
        })
        uploadRequest('images/upload', payload)
            .then((response) => {
                if(!response.status) {
                    dispatch({
                        type: constants.UPLOAD_COMPLETE
                    })
                } else {
                    dispatch({
                        type: constants.UPLOAD_FAIL,
                        payload: response.message
                    })
                }
            })
    }
}

const uploadInit = () => dispatch => dispatch({type: constants.UPLOAD_INIT})

const setfavourites = payload => {
    return dispatch => {
        postRequest('favourites', JSON.stringify({ "image_id" : payload}))
            .then((response) => {
                dispatch({
                    type: constants.SETFAVOURITES,
                    payload: {
                        ...response,
                        "image_id" : payload
                    }
                })
            })
    }
}

const unFavourites = payload => {
    return dispatch => {
        deleteRequest(`favourites/${payload}`)
            .then(() => {
                dispatch({
                    type: constants.UNFAVOURITES,
                    payload: payload               })
            })
    }
}

const getFavourites = () => {
    return dispatch => {
        getRequest('favourites')
            .then(response => {
                dispatch({
                    type: constants.FAVOURITES,
                    payload: response
                })
            })
    }
}

const addVote = payload => {
    return dispatch => {
        postRequest('votes', JSON.stringify({ "image_id" : payload, "value": 1}))
            .then((response) => {
                dispatch({
                    type: constants.ADD_VOTE,
                    payload: {
                        ...response,
                        "image_id" : payload
                    }
                })
            })
    }
}

const deleteVote = payload => {
    return dispatch => {
        deleteRequest(`votes/${payload}`)
            .then(() => {
                dispatch({
                    type: constants.DELETE_VOTE,
                    payload: payload               })
            })
    }
}

const getVotes = () => {
    return dispatch => {
        getRequest('votes')
            .then(response => {
                dispatch({
                    type: constants.VOTES,
                    payload: response
                })
            })
    }
}

export {
    getCatList,
    uploadImage,
    uploadInit,
    getFavourites,
    unFavourites,
    setfavourites,
    addVote,
    deleteVote,
    getVotes
}