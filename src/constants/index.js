
export const CAT_LIST = 'CAT_LIST';
export const CAT_LIST_INIT= 'CAT_LIST_INIT';
export const UPLOAD_COMPLETE = 'UPLOAD_COMPLETE';
export const UPLOAD_FAIL = 'UPLOAD_FAIL';
export const UPLOAD_INIT = 'UPLOAD_INIT';
export const FAVOURITES = 'FAVOURITES';
export const SETFAVOURITES = 'SETFAVOURITES';
export const UNFAVOURITES = 'UNFAVOURITES';
export const VOTES = 'VOTES';
export const ADD_VOTE = 'ADD_VOTES';
export const DELETE_VOTE = 'DELETE_VOTES';
export const STAGE = {
    init: 0,
    completed: 1,
    fail: 2
};