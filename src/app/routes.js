import React from 'react';
import { Switch, Route } from 'react-router-dom';
import HomePage from './../components/pages/HomePage';
import UploadPage from './../components/pages/UploadPage';

const routes = (
    <Switch>
        <Route path="/" exact component={HomePage} />
        <Route path="/upload" component={UploadPage} />
    </Switch>
)

export default routes;