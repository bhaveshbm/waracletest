import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import routes from './routes';

const history = createBrowserHistory();
const middleware = [thunk];
const composeFunction = [applyMiddleware(...middleware)];
const store = createStore(
        rootReducer,
        {},
        compose(
            ...composeFunction
        )
    );

const App = () => <Provider store={store}>
    <Router history={history}>{routes}</Router>
</Provider>

export default App;