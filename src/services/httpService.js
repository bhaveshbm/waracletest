const ENDPOINT = "https://api.thecatapi.com/v1";
const API_KEY = "c29e0774-4028-4062-874e-9f2c41b47d68";
const requestJson =  async (url, options, optionalHeader = {}) => {
    return new Promise((resolve) => {
        fetch(
            `${ENDPOINT}/${url}`,
            {
                ...options,
                headers: {
                    ...optionalHeader,
                    'Authorization': true,
                    'mode': 'no-cors',
                    'Access-Control-Allow-Origin': '*',
                    'x-api-key': `${API_KEY}`
                }
            }
        )
        .then(response => {
           resolve(response.json())
        })
        .catch(error =>  {
            console.log(error)
        })
    })
}

const getRequest = (url) => requestJson(url, {
    method: 'GET'
})

const uploadRequest = (url, data = {}) => requestJson(url, {
    method: 'POST',
    body: data
});

const postRequest = (url, data = {}) => requestJson(url, {
    method: 'POST',
    body: data
    },
    {'Content-Type': 'application/json' }
);

const deleteRequest = (url) => requestJson(url, {
    method: 'DELETE'
})

export {
    getRequest,
    uploadRequest,
    postRequest,
    deleteRequest
}